CREATE TABLE cliente (
  cli_id   INTEGER       NOT NULL PRIMARY KEY,
  cli_nome VARCHAR(255) NOT NULL UNIQUE,
  cli_cnpj BIGINT       NOT NULL UNIQUE
);

CREATE TABLE statuspedido (
  sta_id   SMALLINT    NOT NULL PRIMARY KEY,
  sta_nome VARCHAR(60) NOT NULL
);

CREATE TABLE pedido (
  ped_id      SERIAL      NOT NULL PRIMARY KEY,
  ped_assunto VARCHAR(60) NOT NULL,
  cli_id      INTEGER     NOT NULL REFERENCES cliente (cli_id),
  sta_id      INTEGER     NOT NULL REFERENCES statuspedido (sta_id)
);

CREATE TABLE historicostatuspedido (
  his_id        SERIAL   NOT NULL PRIMARY KEY,
  ped_id 		INTEGER NOT NULL REFERENCES pedido(ped_id),
  his_datahora  timestamp without time zone   NOT NULL DEFAULT now(),
  sta_id_novo   SMALLINT NOT NULL REFERENCES statuspedido (sta_id),
  sta_id_antigo SMALLINT REFERENCES statuspedido (sta_id)
);


INSERT INTO cliente (cli_id, cli_nome, cli_cnpj)
VALUES (1, 'VELTEC S.A', 7550731000150);

INSERT INTO statuspedido (sta_id, sta_nome)
VALUES (1, 'Orçamento'), (2, 'Pendente Aprovação'), (3, 'Aprovado');

CREATE OR REPLACE FUNCTION gerar_historico_status_pedido()
  RETURNS TRIGGER AS
$BODY$
BEGIN
  IF TG_OP = 'INSERT'
  THEN
    INSERT INTO historicostatuspedido (ped_id, sta_id_novo)
    VALUES (new.ped_id, new.sta_id);
  ELSIF TG_OP = 'UPDATE' AND (new.sta_id <> old.sta_id)
    THEN
      INSERT INTO historicostatuspedido (ped_id, sta_id_novo, sta_id_antigo)
      VALUES (new.ped_id, new.sta_id, old.sta_id);
  END IF;

  RETURN new;
END;
$BODY$
LANGUAGE plpgsql VOLATILE SECURITY DEFINER
COST 100;

CREATE TRIGGER tg_gerar_historico_status_pedido
AFTER INSERT OR UPDATE ON pedido
FOR EACH ROW
EXECUTE PROCEDURE gerar_historico_status_pedido();