Teste PL/SQL e Java
================
Este teste prático tem por objetivo a execução de práticas do dia a dia desempenhadas nos sistemas internos da Veltec. Ele contém um script SQL que cria uma base de dados para movimentação de pedidos e uma sequência de exercícios a serem resolvidos utilizando o SGBD [PostgreSQL](https://www.postgresql.org) e a linguagem [Java SE](http://www.oracle.com/technetwork/java/javase/overview/index.html).

1. Crie um novo banco de dados e aplique o script base_dados_pedidos.sql

1. Implemente uma stored procedure na linguagem [PL/pgSQL](https://www.postgresql.org/docs/current/static/plpgsql.html) que salve um pedido, recebendo como parâmetro todos os campos de sua tabela, e que retorne seu ID (PK). Caso o ID enviado como parâmetro seja nulo, deve-se inserir um novo registro, caso contrário, atualizar os campos do registro correspondente ao ID.

1. Em Java, implemente uma rotina que realize as cinco chamadas abaixo na stored procedure implementada no exercício anterior:

	1. Inserção de um novo pedido para o cliente "VELTEC S.A" com status "Orçamento"
	1. Atualização do status do pedido inserido para "Pendente Aprovação"
	1. Atualização do status do pedido inserido para "Orçamento"
	1. Atualização do status do pedido inserido para "Pendente Aprovação"
	1. Atualização do status do pedido inserido para "Aprovado"

1. Sabendo que o script SQL de criação da base de dados contém um [trigger](https://www.postgresql.org/docs/9.3/static/plpgsql-trigger.html) que alimenta uma tabela de histórico ao realizar inserção ou atualização de status na tabela de pedidos, desenvolva uma segunda rotina em Java que selecione e exiba na tela (console) o registro (ID, assunto, e data/hora) mais recente em que o pedido foi alterado para o status "Pendente Aprovação".

1. Forneça o código e a(s) chamada(s) Java necessárias para executarmos suas rotinas e visualizarmos o resultado da consulta implementada no exercício anterior.